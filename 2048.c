#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "jeu.h"
#include "saisieM.h"

int main()
{
    jeu *p;
    srand(time(NULL));
    p=(jeu*) malloc(sizeof(jeu)); //Allocation dynamique de p
    initialiseJeu(p,4,2048);   //Initialise les parametres du jeu (grille, valmax, dimension)
    menu(p);
    libereMemoire(p); //Libere la memoire de p apr�s son allocation dynamique
    //return 0;
}
