#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"

typedef struct //Q1
{
    int n;
    int ValMax;
    int *grille;
    int nbCasesLibres;
}jeu;

void initialiseJeu(jeu *p, int n, int ValMax) //Q2
{
    int i;
    p->n=n;
    p->ValMax=ValMax;
    p->nbCasesLibres=p->n*p->n;
    printf("%d cases allouees ... \n",(p->n)*(p->n));
    (*p).grille=(int*)malloc((p->n)*(p->n)*sizeof(int)); //Allocation dynamique de la grille
    for(i=0;i<(p->n)*(p->n);i++)
    {
        p->grille[i]=0;
    }
}

void libereMemoire(jeu *p) //Q3
{
    free(p);
    (*p).grille=NULL;
}

int indiceValide (jeu *p, int i, int j) //Q4 - v�rifie si la case est valide, si oui renvoie 1 sinon 0
{
    if((i>=0)&&(i<(p->n))&&(j>=0) && (j<(p->n)))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int getVal (jeu *p, int ligne, int colonne)  //Q5 - recupere une valeur selon la ligne et la colonne
{
    if((indiceValide(p,ligne,colonne))== 1)
	{
        return p->grille[((ligne*(p->n))+colonne)]; //Une ligne a n colonne, donc on multiplie ligne par n pour avoir la premiere case de la ligne voulu. On lui ajoute ensuite le numero de la colonne.
	}
	else
	{
        return -1;
	}
}

void setVal (jeu *p, int ligne, int colonne, int val)   //Q6 - modifie la valeur de la case donn�.
{
    if((indiceValide(p,ligne,colonne))==1)
	{
        p->grille[((ligne*(p->n))+colonne)]= val;
	}
}

void affichage_simple(jeu *p) //Q7 - Affiche ligne par ligne le tableau
{
    int i;
    int nbcases=((*p).n)*((*p).n);
    int restart=(*p).n;

    for(i=0;i<nbcases;i++)
    {
        if(i%restart==0)
        {
            printf("\n"); //Saute une ligne quand compteur modulo dimension = 0
        }
        printf("%5d",p->grille[i]);
    }

}

void affichage_moyen(jeu *p) //Q7 - Affiche ligne par ligne le tableau
{
    int i;
    int j;
    char etoile='*';
    for(i=0;i<(((*p).n)*((*p).n));i++)//De la case en haut � gauche vers la case en bas a droite (0,0)->(n,n)
    {
        if(i%(*p).n==0)
        {
            printf("\n");//Saute une ligne en atteignant (0,n)
            printf("%5c",etoile);//Affiche la premiere etoile d'une ligne
            for(j=0;j<2*((*p).n);j++)
            {
                printf("*****");//Affiche n fois 5 etoiles
            }
            printf("\n");//Saute une ligne apr�s avoir fait une ligne complete d'etoiles
            printf("%5c",etoile);//Affiche la premiere etoile d'une ligne
        }
        printf("%5d",p->grille[i]);//Affiche p.grille[i] sur 5 caracteres
        printf("%5c",etoile);
    }
    printf("\n");//Saute une ligne en atteignant (n-1,n)
    printf("    ");
    for(i=0;i<((*p).n)*2;i++)//Affiche la derniere ligne d'etoile
    {
        printf("*****");
    }
    printf("* \n");

}

int caseVide(jeu *p, int i, int j) //Q8 - Retourne 1 si la case (i,j) est vide, 0 sinon
{
    if(getVal(p,i,j)==-1) //Si getVal=-1 cad si la case est non valide
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void ajouteValAlea(jeu *p) //Q9 - Ajoute 2 ou 4 � une case libre al�atoirement
{
    int i;
    int casevide=0;
    int nouvellecase;
    if((rand()%10+1)%2==0) //Tirage au sort du 2 ou 4
    {
        nouvellecase=2;
    }
    else
    {
        nouvellecase=4;
    }
    for(i=0;i<((*p).n)*((*p).n);i++)
    {
        if(p->grille[i]==0)
        {
            casevide++;
        }
    }
    if(casevide>0) //On tire au sort l'indice de la case libre
    {
        for(i=0;i<((*p).n)*((*p).n);i++)
        {
            if(p->grille[rand()%(*p).n*(*p).n]==0) //On verifie que la case est egale � 0
            {
                p->grille[i]=nouvellecase;
            }
        }
    }
}

int gagne(jeu *p) //Q10
{
    int i;
    for(i=0;i<((*p).n)*((*p).n);i++)
    {
        if(p->grille[i]>=(*p).ValMax) //Si une valeur atteint ValMax ou plus
        {
            return 1;
        }
    }
    return 0;
}

int perdu(jeu *p) //Q11 - Renvoi 1 si partie perdu, 0 sinon
{
    int i;
    int casevide=0;
    for(i=0;i<((*p).n)*((*p).n);i++) //Compte le nombre de case vide
    {
        if(p->grille[i]==0)
        {
            casevide++;
        }
    }
    if(casevide==0) //Si aucune case vide
    {
        for(i=0;i<((*p).n)*((*p).n);i++) //Et si aucune case adjacente n'est de meme valeur
        {
            if(p->grille[i+1]==p->grille[i] || p->grille[i-1]==p->grille[i] || p->grille[i+(*p).n]==p->grille[i] || p->grille[i-(*p).n]==p->grille[i])
            {
                return 0;
            }
        }
        return 1;
    }
}

int finPartie(jeu *p) //Q12 - Renvoi 1 si partie finie, 0 sinon
{
    if(perdu(p)==1 || gagne(p)==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


int mouvementLigne(jeu *p, int ligne, int direction) //Q13
{
    int indice_colonne;
    int move=0;
    if(direction==1) //Si gauche, on commence au debut
    {
        indice_colonne=0;
    }
    else if(direction==-1) //Si droite, on commence par la fin
    {
        indice_colonne=((*p).n)-1;
    }
    while((direction==-1) && (indice_colonne>=0)) // Droite et pas au bout de la ligne
    {
        if((getVal(p,ligne,indice_colonne)!=0) && ((p->grille[(((p->n)*ligne)+indice_colonne)])==(p->grille[(((p->n)*ligne)+indice_colonne)-1])))
        { // Addition (0,0,2,2) -> (0,0,0,4)
            p->grille[(((p->n)*ligne)+indice_colonne)]=(p->grille[(((p->n)*ligne)+indice_colonne)])*2; // (0,0,2,4)
            p->grille[(((p->n)*ligne)+indice_colonne)-1]=0; // (0,0,0,4)
            move++; //On a deplac� une case
        }
        indice_colonne--;
    }
    indice_colonne=((*p).n)-1; //Reinitialise a fin de ligne
    while((direction==-1) && (indice_colonne>=0)) // Droite et pas au bout de la ligne
    {
        if((getVal(p,ligne,indice_colonne)<=0) && (p->grille[(((p->n)*ligne)+indice_colonne)-1])>0 && (p->grille[(((p->n)*ligne)+indice_colonne)-1]<=p->ValMax))
        { // Deplacement (2,4,0,0) -> (0,0,2,4)
            p->grille[(((p->n)*ligne)+indice_colonne)+1]=p->grille[(((p->n)*ligne)+indice_colonne)]; // (2,4,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)]=0; // (2,0,4,0)
            move++; //On a deplac� une case
        }
        indice_colonne--;
    }
    indice_colonne=0; //Reinitialise au debut de ligne
    while((direction==1) && (indice_colonne<(*p).n)) // Gauche et pas au bout de la ligne
    {
        if((getVal(p,ligne,indice_colonne))!=0 && ((p->grille[(((p->n)*ligne)+indice_colonne)])==(p->grille[(((p->n)*ligne)+indice_colonne)+1])))
        {  // Addition (2,2,0,0) -> (4,0,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)]=p->grille[(((p->n)*ligne)+indice_colonne)+1]*2; // (4,2,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)+1]=0; // (4,0,0,0)
            move++; //On a deplac� une case
        }
        indice_colonne++;
    }
    indice_colonne=0; //Reinitialise au debut de ligne
    while((direction==1) && (indice_colonne<(*p).n)) // Gauche et pas au bout de la ligne
    {
        if(getVal(p,ligne,indice_colonne)<=0)
        { //Deplacement  (0,2,0,0) -> (2,0,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)]=p->grille[(((p->n)*ligne)+indice_colonne)+1]; //(2,2,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)+1]=0; //(2,0,0,0)
            move++; //On a deplac� une case
        }
        indice_colonne++;
    }
    if(move>0)
    {
        return 1; //Si on a deplac� une case, retourne 1
    }
    else
    {
        return 0;
    }
}

int mouvementLignes(jeu *p, int direction) //Q14
{
    int i; int move=0;
    for(i=0;i<(p->n);i++) //mouvementLigne de colonne=0 a colonne=(p->n)
    {
        mouvementLigne(p,i,direction);
        if(mouvementLigne(p,i,direction==1)) //Si mouvementLigne retourne 1 une fois au moins, move=1
        {
            move=1;
        }
    }
    return move;
}


int mouvementColonne(jeu *p, int colonne, int direction) //Q15
{
    int indice_ligne; int move;
    if(direction==1)
    {
        indice_ligne=0;
    }
    else if(direction==-1)
    {
        indice_ligne=(p->n)-1;
    }
    while((direction==1) && (indice_ligne > 0)) //vers le haut & addition
    {
        if((getVal(p,indice_ligne,colonne)!=0) && getVal(p,indice_ligne,colonne)==getVal(p,indice_ligne-1,colonne))
            {
                setVal(p,indice_ligne-1,colonne,(getVal(p,indice_ligne-1,colonne)*2));
                setVal(p,indice_ligne,colonne,0);
                move++; //On a deplac� une case
            }
            indice_ligne--;
    }
    indice_ligne=(p->n)-1;
    while((direction==1) && (indice_ligne>0)) //vers le haut et deplacement 0
    {
        if(getVal(p,indice_ligne-1,colonne)==0 && getVal(p,indice_ligne,colonne)!=0)
        {
            setVal(p,indice_ligne-1,colonne,getVal(p,indice_ligne,colonne));
            setVal(p,indice_ligne,colonne,0);
            move++; //On a deplac� une case
        }
        indice_ligne--;
    }
    indice_ligne=0;
    while((direction==-1) && (indice_ligne<(p->n))) //vers le bas et addition
    {
        if((getVal(p,indice_ligne,colonne)==getVal(p,indice_ligne+1,colonne)) && (getVal(p,indice_ligne,colonne)!=0))
        {
            setVal(p,indice_ligne,colonne,(getVal(p,indice_ligne,colonne))*2);
            setVal(p,indice_ligne+1,colonne,0);
            move++; //On a deplac� une case
        }
        indice_ligne++;
    }
    indice_ligne=0;
    while((direction==-1) && (indice_ligne)<(p->n)) //vers le bas et deplacement 0
    {
        if(getVal(p,indice_ligne,colonne!=0) && getVal(p,indice_ligne+1,colonne)==0)
        {
            setVal(p,indice_ligne+1,colonne,(getVal(p,indice_ligne,colonne)));
            setVal(p,indice_ligne,colonne,0);
            move++; //On a deplac� une case
        }
        indice_ligne++;
    }
    if(move>0)
    {
        return 1; //Si on a deplac� une case, retourne 1
    }
    else
    {
        return 0;
    }

}

int mouvementColonnes(jeu *p, int direction) //Q16
{
    int i; int move=0;
    for(i=0;i<(p->n);i++) //mouvementColonne de ligne=0 a ligne=(p->n)
    {
        mouvementColonne(p,i,direction);
        if(mouvementColonne(p,i,direction)==1)
        {
            move=1;
        }
    }
    return move;
}

int mouvement(jeu *p, int direction) //Q17
{
    int move=0;
    if(direction==0) //0 vers le bas
    {
        mouvementColonnes(p,-1); //On execute un mouvement en bas
        if(mouvementColonnes(p,-1)==1)
        {
            move=1;
        }
    }
    else if(direction==1) //1 vers la droite
    {
        mouvementLignes(p,-1); //On execute un mouvement a droite
        if(mouvementLignes(p,-1)==1)
        {
            move=1;
        }
    }
    else if(direction==2) //2 vers le haut
    {
       mouvementColonnes(p,1); //On execute un mouvement en haut
        if(mouvementColonnes(p,1)==1)
        {
            move=1;
        }
    }
    else if(direction==3) //3 vers la gauche
    {
       mouvementLignes(p,1); //On execute un mouvement a gauche
        if(mouvementLignes(p,1)==1)
        {
            move=1;
        }
    }
    return move; //Retourne 1 si mouvement 0 sinon
}

int saisieD() //Q18
{
    Key touche; int move;
    touche=lectureFleche(); //Avt les tests
    debutTerminalSansR(); //Ouvre le mode saisie
    if(touche==KEY_UP) //Appel mouvementColonnes
    {
        move=2;
    }
    if(touche==KEY_DOWN) //Appel mouvementColonnes
    {
        move=0;
    }
    if(touche==KEY_LEFT) //Appel mouvementLignes
    {
        move=3;
    }
    if(touche==KEY_RIGHT) //Appel mouvementLignes
    {
        move=1;
    }
    if(touche==KEY_ESCAPE) //Termine le jeu
    {
        return -1;
    }
    else if(touche==NO_KEY) //Attend une nouvelle saisie
    {
        printf("Saisissez Haut / Bas / Gauche / Droite pour jouer || Esc pour quitter le jeu\n");
    }

    finTerminalSansR();
    return move;
}

int jouer(jeu *p) //Q19
{
    int direction=0;
    while(finPartie(p)==0 && direction!=-1) //Tant que la partie n'est pas finie et que Esc n'est pas activ�
    {
        ajouteValAlea(p);
        affichage_moyen(p);
        direction=saisieD();
    }
    affichage_moyen(p);
    if(finPartie(p)==1 || saisieD()==-1)
    {
        return 1; //Partie termin�e
    }
    else
    {
        return 0; //Partie non termin�e
    }
}

int sauvegarde(jeu *p) //Q20
{
    int i=0;
    FILE *fichier;
    fichier=fopen("sauvegarde.txt","wt");
    if(fichier==NULL)
    {
        fclose(fichier);
        return 0;
    }
    else
    {
        fprintf(fichier,"%d %d %d ",p->n,p->ValMax,p->nbCasesLibres);
        while(i<((*p).n)*((*p).n))
        {
            fprintf(fichier,"%d ",p->grille[i]);
            i++;
        }
        fclose(fichier);
        return 1;
    }
}

int chargement(jeu *p) //Q20
{
    int i=0;
    FILE *fichier;
    fichier=fopen("sauvegarde.txt","rt");
    if(fichier=NULL)
    {
        fclose(fichier);
        return 0;
    }
    else
    {
        fscanf(fichier,"%d %d %d ",p->n,p->ValMax,p->nbCasesLibres);
        while(i<((*p).n)*((*p).n))
        {
            fscanf(fichier,"%d ",p->grille[i]);
            i++;
        }
        fclose(fichier);
        return 1;
    }
}

int menu(jeu *p) //Q21
{
    int i=0;
    while(i=0 || jouer(p)==0) //Tant qu'on a pas saisie ou quand la partie est termin�e
    {
        printf("1-Jouer \n 2-Sauvegarder \n 3-Charger \n 4-Quitter \n");
        scanf("%d",&i);
        if(i==1)
        {
            jouer(p); //lance le jeu
        }
        if(i==2)
        {
            sauvegarde(p);
        }
        if(i==3)
        {
            chargement(p); //Charge de sauvegarde.txt et lance le jeu
            jouer(p);
        }
        if(i==4)
        {
            exit(1);
        }
        else
        {
            printf("Saisissez un nombre entre 1 et 4 selon le menu");
        }
    }
}

int main()
{
    jeu *p;
    srand(time(NULL));
    p=(jeu*) malloc(sizeof(jeu)); //Allocation dynamique de p
    initialiseJeu(p,4,2048);   //Initialise les parametres du jeu (grille, valmax, dimension)
    menu(p);
    libereMemoire(p); //Libere la memoire de p apr�s son allocation dynamique
    //return 0;
}

