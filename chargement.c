#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int chargement(jeu *p)
{
    int i=0;
    FILE *fichier;
    fichier=fopen("sauvegarde.txt","rt");
    if(fichier=NULL)
    {
        fclose(fichier);
        return 0;
    }
    else
    {
        fscanf(fichier,"%d %d %d ",p->n,p->ValMax,p->nbCasesLibres);
        while(i<((*p).n)*((*p).n))
        {
            fscanf(fichier,"%d ",p->grille[i]);
            i++;
        }
        fclose(fichier);
        return 1;
    }
}
