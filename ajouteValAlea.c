#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

void ajouteValAlea(jeu *p) //Q9 - Ajoute 2 ou 4 � une case libre al�atoirement
{
    int i;
    int casevide=0;
    int nouvellecase;
    if((rand()%10+1)%2==0) //Tirage au sort du 2 ou 4
    {
        nouvellecase=2;
    }
    else
    {
        nouvellecase=4;
    }
    for(i=0;i<((*p).n)*((*p).n);i++)
    {
        if(p->grille[i]==0)
        {
            casevide++;
        }
    }
    if(casevide>0) //On tire au sort l'indice de la case libre
    {
        for(i=0;i<((*p).n)*((*p).n);i++)
        {
            if(p->grille[rand()%(*p).n*(*p).n]==0) //On verifie que la case est egale � 0
            {
                p->grille[i]=nouvellecase;
            }
        }
    }
}
