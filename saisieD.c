#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int saisieD() //Q18
{
    Key touche; int move;
    touche=lectureFleche(); //Avt les tests
    debutTerminalSansR(); //Ouvre le mode saisie
    if(touche==KEY_UP) //Appel mouvementColonnes
    {
        move=2;
    }
    if(touche==KEY_DOWN) //Appel mouvementColonnes
    {
        move=0;
    }
    if(touche==KEY_LEFT) //Appel mouvementLignes
    {
        move=3;
    }
    if(touche==KEY_RIGHT) //Appel mouvementLignes
    {
        move=1;
    }
    if(touche==KEY_ESCAPE) //Termine le jeu
    {
        return -1;
    }
    else if(touche==NO_KEY) //Attend une nouvelle saisie
    {
        printf("Saisissez Haut / Bas / Gauche / Droite pour jouer || Esc pour quitter le jeu\n");
    }

    finTerminalSansR();
    return move;
}
