#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int perdu(jeu *p) //Q11 - Renvoi 1 si partie perdu, 0 sinon
{
    int i;
    int casevide=0;
    for(i=0;i<((*p).n)*((*p).n);i++) //Compte le nombre de case vide
    {
        if(p->grille[i]==0)
        {
            casevide++;
        }
    }
    if(casevide==0) //Si aucune case vide
    {
        for(i=0;i<((*p).n)*((*p).n);i++) //Et si aucune case adjacente n'est de meme valeur
        {
            if(p->grille[i+1]==p->grille[i] || p->grille[i-1]==p->grille[i] || p->grille[i+(*p).n]==p->grille[i] || p->grille[i-(*p).n]==p->grille[i])
            {
                return 0;
            }
        }
        return 1;
    }
}
