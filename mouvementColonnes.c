#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int mouvementColonnes(jeu *p, int direction) //Q16
{
    int i; int move=0;
    for(i=0;i<(p->n);i++) //mouvementColonne de ligne=0 a ligne=(p->n)
    {
        mouvementColonne(p,i,direction);
        if(mouvementColonne(p,i,direction)==1)
        {
            move=1;
        }
    }
    return move;
}
