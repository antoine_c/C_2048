#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

void initialiseJeu(jeu *p, int n, int ValMax) //Q2
{
    int i;
    p->n=n;
    p->ValMax=ValMax;
    p->nbCasesLibres=p->n*p->n;
    printf("%d cases allouees ... \n",(p->n)*(p->n));
    (*p).grille=(int*)malloc((p->n)*(p->n)*sizeof(int)); //Allocation dynamique de la grille
    for(i=0;i<(p->n)*(p->n);i++)
    {
        p->grille[i]=0;
    }
}
