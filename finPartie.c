#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int finPartie(jeu *p) //Q12 - Renvoi 1 si partie finie, 0 sinon
{
    if(perdu(p)==1 || gagne(p)==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
