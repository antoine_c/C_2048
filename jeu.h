#ifndef JEU_H
#define JEU_H

typedef struct
{
    int n;
    int ValMax;
    int *grille;
    int nbCasesLibres;
}jeu;

void initialiseJeu(jeu *p, int n, int ValMax);
void libereMemoire(jeu *p);
int indiceValide (jeu *p, int i, int j);
int getVal (jeu *p, int ligne, int colonne);
void setVal (jeu *p, int ligne, int colonne, int val);
void affichage_simple(jeu *p);
void affichage_moyen(jeu *p);
int caseVide(jeu *p, int i, int j);
void ajouteValAlea(jeu *p);
int gagne(jeu *p);
int perdu(jeu *p);
int finPartie(jeu *p);
int mouvementLigne(jeu *p, int ligne, int direction);
int mouvementLignes(jeu *p, int direction);
int mouvementColonne(jeu *p, int colonne, int direction);
int mouvementColonnes(jeu *p, int direction);
int mouvement(jeu *p, int direction);
int saisieD();
int jouer(jeu *p);
int sauvegarde(jeu *p);
int chargement(jeu *p);
void menu(jeu *p);

#endif
