#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

void affichage_moyen(jeu *p) //Q7 - Affiche ligne par ligne le tableau
{
    int i;
    int j;
    char etoile='*';
    for(i=0;i<(((*p).n)*((*p).n));i++)//De la case en haut � gauche vers la case en bas a droite (0,0)->(n,n)
    {
        if(i%(*p).n==0)
        {
            printf("\n");//Saute une ligne en atteignant (0,n)
            printf("%5c",etoile);//Affiche la premiere etoile d'une ligne
            for(j=0;j<2*((*p).n);j++)
            {
                printf("*****");//Affiche n fois 5 etoiles
            }
            printf("\n");//Saute une ligne apr�s avoir fait une ligne complete d'etoiles
            printf("%5c",etoile);//Affiche la premiere etoile d'une ligne
        }
        printf("%5d",p->grille[i]);//Affiche p.grille[i] sur 5 caracteres
        printf("%5c",etoile);
    }
    printf("\n");//Saute une ligne en atteignant (n-1,n)
    printf("    ");
    for(i=0;i<((*p).n)*2;i++)//Affiche la derniere ligne d'etoile
    {
        printf("*****");
    }
    printf("* \n");

}
