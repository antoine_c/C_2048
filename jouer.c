#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int jouer(jeu *p) //Q19
{
    int direction=0;
    while(finPartie(p)==0 && direction!=-1) //Tant que la partie n'est pas finie et que Esc n'est pas activ�
    {
        ajouteValAlea(p);
        affichage_moyen(p);
        direction=saisieD();
    }
    affichage_moyen(p);
    if(finPartie(p)==1 || saisieD()==-1)
    {
        return 1; //Partie termin�e
    }
    else
    {
        return 0; //Partie non termin�e
    }
}
