#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int getVal (jeu *p, int ligne, int colonne)  //Q5 - recupere une valeur selon la ligne et la colonne
{
    if((indiceValide(p,ligne,colonne))== 1)
	{
        return p->grille[((ligne*(p->n))+colonne)]; //Une ligne a n colonne, donc on multiplie ligne par n pour avoir la premiere case de la ligne voulu. On lui ajoute ensuite le numero de la colonne.
	}
	else
	{
        return -1;
	}
}
