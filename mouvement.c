#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int mouvement(jeu *p, int direction) //Q17
{
    int move=0;
    if(direction==0) //0 vers le bas
    {
        mouvementColonnes(p,-1); //On execute un mouvement en bas
        if(mouvementColonnes(p,-1)==1)
        {
            move=1;
        }
    }
    else if(direction==1) //1 vers la droite
    {
        mouvementLignes(p,-1); //On execute un mouvement a droite
        if(mouvementLignes(p,-1)==1)
        {
            move=1;
        }
    }
    else if(direction==2) //2 vers le haut
    {
       mouvementColonnes(p,1); //On execute un mouvement en haut
        if(mouvementColonnes(p,1)==1)
        {
            move=1;
        }
    }
    else if(direction==3) //3 vers la gauche
    {
       mouvementLignes(p,1); //On execute un mouvement a gauche
        if(mouvementLignes(p,1)==1)
        {
            move=1;
        }
    }
    return move; //Retourne 1 si mouvement 0 sinon
}
