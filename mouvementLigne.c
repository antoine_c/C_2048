#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int mouvementLigne(jeu *p, int ligne, int direction) //Q13
{
    int indice_colonne;
    int move=0;
    if(direction==1) //Si gauche, on commence au debut
    {
        indice_colonne=0;
    }
    else if(direction==-1) //Si droite, on commence par la fin
    {
        indice_colonne=((*p).n)-1;
    }
    while((direction==-1) && (indice_colonne>=0)) // Droite et pas au bout de la ligne
    {
        if((getVal(p,ligne,indice_colonne)!=0) && ((p->grille[(((p->n)*ligne)+indice_colonne)])==(p->grille[(((p->n)*ligne)+indice_colonne)-1])))
        { // Addition (0,0,2,2) -> (0,0,0,4)
            p->grille[(((p->n)*ligne)+indice_colonne)]=(p->grille[(((p->n)*ligne)+indice_colonne)])*2; // (0,0,2,4)
            p->grille[(((p->n)*ligne)+indice_colonne)-1]=0; // (0,0,0,4)
            move++; //On a deplac� une case
        }
        indice_colonne--;
    }
    indice_colonne=((*p).n)-1; //Reinitialise a fin de ligne
    while((direction==-1) && (indice_colonne>=0)) // Droite et pas au bout de la ligne
    {
        if((getVal(p,ligne,indice_colonne)<=0) && (p->grille[(((p->n)*ligne)+indice_colonne)-1])>0 && (p->grille[(((p->n)*ligne)+indice_colonne)-1]<=p->ValMax))
        { // Deplacement (2,4,0,0) -> (0,0,2,4)
            p->grille[(((p->n)*ligne)+indice_colonne)+1]=p->grille[(((p->n)*ligne)+indice_colonne)]; // (2,4,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)]=0; // (2,0,4,0)
            move++; //On a deplac� une case
        }
        indice_colonne--;
    }
    indice_colonne=0; //Reinitialise au debut de ligne
    while((direction==1) && (indice_colonne<(*p).n)) // Gauche et pas au bout de la ligne
    {
        if((getVal(p,ligne,indice_colonne))!=0 && ((p->grille[(((p->n)*ligne)+indice_colonne)])==(p->grille[(((p->n)*ligne)+indice_colonne)+1])))
        {  // Addition (2,2,0,0) -> (4,0,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)]=p->grille[(((p->n)*ligne)+indice_colonne)+1]*2; // (4,2,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)+1]=0; // (4,0,0,0)
            move++; //On a deplac� une case
        }
        indice_colonne++;
    }
    indice_colonne=0; //Reinitialise au debut de ligne
    while((direction==1) && (indice_colonne<(*p).n)) // Gauche et pas au bout de la ligne
    {
        if(getVal(p,ligne,indice_colonne)<=0)
        { //Deplacement  (0,2,0,0) -> (2,0,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)]=p->grille[(((p->n)*ligne)+indice_colonne)+1]; //(2,2,0,0)
            p->grille[(((p->n)*ligne)+indice_colonne)+1]=0; //(2,0,0,0)
            move++; //On a deplac� une case
        }
        indice_colonne++;
    }
    if(move>0)
    {
        return 1; //Si on a deplac� une case, retourne 1
    }
    else
    {
        return 0;
    }
}
