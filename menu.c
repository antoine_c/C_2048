#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int menu(jeu *p)
{
    int i=0;
    while(i=0 || jouer(p)==0) //Tant qu'on a pas saisie ou quand la partie est termin�e
    {
        printf("1-Jouer \n 2-Sauvegarder \n 3-Charger \n 4-Quitter \n");
        scanf("%d",&i);
        if(i==1)
        {
            jouer(p); //lance le jeu
        }
        if(i==2)
        {
            sauvegarde(p);
        }
        if(i==3)
        {
            chargement(p); //Charge de sauvegarde.txt et lance le jeu
            jouer(p);
        }
        if(i==4)
        {
            exit(1);
        }
        else
        {
            printf("Saisissez un nombre entre 1 et 4 selon le menu");
        }
    }
}
