#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

void affichage_simple(jeu *p) //Q7 - Affiche ligne par ligne le tableau
{
    int i;
    int nbcases=((*p).n)*((*p).n);
    int restart=(*p).n;

    for(i=0;i<nbcases;i++)
    {
        if(i%restart==0)
        {
            printf("\n"); //Saute une ligne quand compteur modulo dimension = 0
        }
        printf("%5d",p->grille[i]);
    }
}
