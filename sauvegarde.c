#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int sauvegarde(jeu *p)
{
    int i=0;
    FILE *fichier;
    fichier=fopen("sauvegarde.txt","wt");
    if(fichier==NULL)
    {
        fclose(fichier);
        return 0;
    }
    else
    {
        fprintf(fichier,"%d %d %d ",p->n,p->ValMax,p->nbCasesLibres);
        while(i<((*p).n)*((*p).n))
        {
            fprintf(fichier,"%d ",p->grille[i]);
            i++;
        }
        fclose(fichier);
        return 1;
    }
}
