#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int caseVide(jeu *p, int i, int j) //Q8 - Retourne 1 si la case (i,j) est vide, 0 sinon
{
    if(getVal(p,i,j)==-1) //Si getVal=-1 cad si la case est non valide
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
