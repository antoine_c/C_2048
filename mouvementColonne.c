#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int mouvementColonne(jeu *p, int colonne, int direction) //Q15
{
    int indice_ligne; int move;
    if(direction==1)
    {
        indice_ligne=0;
    }
    else if(direction==-1)
    {
        indice_ligne=(p->n)-1;
    }
    while((direction==1) && (indice_ligne > 0)) //vers le haut & addition
    {
        if((getVal(p,indice_ligne,colonne)!=0) && getVal(p,indice_ligne,colonne)==getVal(p,indice_ligne-1,colonne))
            {
                setVal(p,indice_ligne-1,colonne,(getVal(p,indice_ligne-1,colonne)*2));
                setVal(p,indice_ligne,colonne,0);
                move++; //On a deplac� une case
            }
            indice_ligne--;
    }
    indice_ligne=(p->n)-1;
    while((direction==1) && (indice_ligne>0)) //vers le haut et deplacement 0
    {
        if(getVal(p,indice_ligne-1,colonne)==0 && getVal(p,indice_ligne,colonne)!=0)
        {
            setVal(p,indice_ligne-1,colonne,getVal(p,indice_ligne,colonne));
            setVal(p,indice_ligne,colonne,0);
            move++; //On a deplac� une case
        }
        indice_ligne--;
    }
    indice_ligne=0;
    while((direction==-1) && (indice_ligne<(p->n))) //vers le bas et addition
    {
        if((getVal(p,indice_ligne,colonne)==getVal(p,indice_ligne+1,colonne)) && (getVal(p,indice_ligne,colonne)!=0))
        {
            setVal(p,indice_ligne,colonne,(getVal(p,indice_ligne,colonne))*2);
            setVal(p,indice_ligne+1,colonne,0);
            move++; //On a deplac� une case
        }
        indice_ligne++;
    }
    indice_ligne=0;
    while((direction==-1) && (indice_ligne)<(p->n)) //vers le bas et deplacement 0
    {
        if(getVal(p,indice_ligne,colonne!=0) && getVal(p,indice_ligne+1,colonne)==0)
        {
            setVal(p,indice_ligne+1,colonne,(getVal(p,indice_ligne,colonne)));
            setVal(p,indice_ligne,colonne,0);
            move++; //On a deplac� une case
        }
        indice_ligne++;
    }
    if(move>0)
    {
        return 1; //Si on a deplac� une case, retourne 1
    }
    else
    {
        return 0;
    }

}
