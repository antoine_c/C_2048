#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "saisieM.h"
#include "jeu.h"

int mouvementLignes(jeu *p, int direction) //Q14
{
    int i; int move=0;
    for(i=0;i<(p->n);i++) //mouvementLigne de colonne=0 a colonne=(p->n)
    {
        mouvementLigne(p,i,direction);
        if(mouvementLigne(p,i,direction==1)) //Si mouvementLigne retourne 1 une fois au moins, move=1
        {
            move=1;
        }
    }
    return move;
}
